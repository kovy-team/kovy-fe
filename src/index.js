import React from 'react'
import ReactDOM from 'react-dom'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import './index.css'
import App from './containers/App'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'
const middleware = [thunk];

let store = createStore(
  reducers,
  applyMiddleware(...middleware)
)


render(
  <Provider store={store}>
    <App />
  </Provider>, document.getElementById('root')
)

