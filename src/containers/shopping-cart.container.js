import React, { Component } from "react";
import ShoppingCart from "../components/shopping-cart";

class ShoppingCartContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <ShoppingCart />
      </div>
    );
  }
}
export default ShoppingCartContainer;
