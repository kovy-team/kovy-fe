// eslint-disable-next-line
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "../App.css";
import React, { Component } from 'react'
import HomeContainer from './home.container'
import ShopContainer from "./shop.container";
import BlogContainer from "./blog.container";
import ContactContainer from "./contact.container";
import LoginContainer from "./login.container";
import RegisterContainer from "./register.container";
import BlogDetailContainer from "./blog-details.container";
import CheckOutContainer from "./check-out.container";
import FaqContainer from "./faq.container";
import ProductContainer from "./product.container";
import ShoppingCartContainer from "./shopping-cart.container";
const App = () => (
  <Router>
    <Switch>
      <Route exact path='/' component={HomeContainer} />
      <Route exact path='/shop' component={ShopContainer} />
      <Route exact path='/blog' component={BlogContainer} />
      <Route exact path='/contact' component={ContactContainer} />
      <Route exact path='/login' component={LoginContainer} />
      <Route exact path='/register' component={RegisterContainer} />
      <Route exact path='/blog-details' component={BlogDetailContainer} />
      <Route exact path='/check-out' component={CheckOutContainer} />
      <Route exact path='/faq' component={FaqContainer} />
      <Route exact path='/product' component={ProductContainer} />
      <Route exact path='/shopping-cart' component={ShoppingCartContainer} />
    </Switch>
  </Router>
)

export default App;
