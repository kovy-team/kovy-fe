import React, { Component } from "react"
import Login from "../components/login"
import axios from 'axios'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as userActions from '../actions/user.action'

class LoginContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      emailLogin: '',
      passwordLogin: '',
      notificationLogin: '',

    }
  }

  isvalidEmail = (email) => {
    if (email === '' || email.indexOf('@') === -1 || email.indexOf('.') === -1)
      return false
    return true
  }
  loginSubmit = async () => {
    if (!this.isvalidEmail(this.state.emailLogin)) {
      this.setState({ notificationLogin: "Email không hợp lệ" })
      return
    } else {
      this.setState({ notificationLogin: '' })
    }
    let res
    try {
      res = await axios.post('http://localhost:8080/user/login', {
        email: this.state.emailLogin,
        password: this.state.passwordLogin,
      })
    }
    catch (err) {
      if (err.response !== undefined) {
        if (err.response.data.msg === "no_registration_confirmation")
          this.setState({ notificationLogin: 'Tài khoản chưa được kích hoạt' })
        else {
          this.setState({ notificationLogin: 'Email hay mật khẩu không hợp lệ' })
        }
      }
      else {
        this.setState({ notificationLogin: 'Đã xảy ra lỗi' })
      }
      return
    }
    this.props.actions.loginSuccess(res.data.token, res.data.user)
    this.props.history.push('/')

  }

  render() {
    return (
      <div>
        <Login
          setEmailogin={(value) => this.setState({ emailLogin: value })}
          setPasswordlogin={(value) => this.setState({ passwordLogin: value })}
          notificationLogin={this.state.notificationLogin}
          loginSubmit={() => this.loginSubmit()}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => {
  return ({
    actions: bindActionCreators(userActions, dispatch)
  })
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer)
