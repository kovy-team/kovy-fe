import React, { Component } from "react";
import Shop from "../components/shop";

class ShopContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Shop />
      </div>
    );
  }
}
export default ShopContainer;
