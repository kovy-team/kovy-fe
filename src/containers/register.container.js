import React, { Component } from "react"
import Register from "../components/register"
import axios from 'axios'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
// import LoginRegister from '../components/login.register/login.register'
import * as userActions from '../actions/user.action'

class RegisterContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      confirm: '',
      notificationRegister: '',
    }
  }

  isvalidPassword = (password) => {
    if (password.length < 6)
      return false
    return true
  }
  isvalidConfirm = (password, confirm) => {
    if (confirm != password)
      return false
    return true
  }
  isvalidEmail = (email) => {
    if (email === '' || email.indexOf('@') === -1 || email.indexOf('.') === -1)
      return false
    return true
  }

  registerSubmit = async () => {
    debugger;
    if (!this.isvalidEmail(this.state.email)) {
      this.setState({ notificationRegister: "Email không hợp lệ" })
      return
    } else {
      this.setState({ notificationRegister: '' })
    }
    if (!this.isvalidPassword(this.state.password)) {
      this.setState({ notificationRegister: 'Mật khẩu không hợp lệ' })
      return
    } else {
      this.setState({ notificationRegister: '' })
    }
    if (!this.isvalidConfirm(this.state.password, this.state.confirm)) {
      this.setState({ notificationRegister: 'Mật khẩu không đúng' })
      return
    } else {
      this.setState({ notificationRegister: '' })
    }
    try {
      await axios.post('http://localhost:8080/user/register', {
        email: this.state.email,
        password: this.state.password,
      })
    }
    catch (err) {
      if (err.response.data.msg === "Email already exist")
        this.setState({ notificationRegister: 'Email đã tồn tại' })
      else
        this.setState({ notificationRegister: 'Đăng ký không thành công' })
      return
    }
    this.setState({ notificationRegister: 'Đăng ký thành công' })
    this.props.history.push('/')
  }

  render() {
    return (
      <div>
        <Register
          setEmail={(value) => this.setState({ email: value })}
          notificationRegister={this.state.notificationRegister}
          setPassword={(value) => this.setState({ password: value })}
          setConfirm={(value) => this.setState({ confirm: value })}
          registerSubmit={() => this.registerSubmit()}
          islogin={this.props.islogin}
          logout={() => this.props.actions.logout()}
          history={this.props.history}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  islogin: state.userReducers.login.islogin
})

const mapDispatchToProps = dispatch => {
  return ({
    actions: bindActionCreators(userActions, dispatch)
  })
}
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterContainer)
