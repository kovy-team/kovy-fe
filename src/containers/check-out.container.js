import React, { Component } from "react";
import CheckOut from "../components/check-out";

class CheckOutContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <CheckOut />
      </div>
    );
  }
}
export default CheckOutContainer;
