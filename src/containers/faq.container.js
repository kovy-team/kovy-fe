import React, { Component } from "react";
import Faq from "../components/faq";

class FaqContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Faq />
      </div>
    );
  }
}
export default FaqContainer;
