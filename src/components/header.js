// eslint-disable-next-line
import React, { Component } from "react";
import { Link } from "react-router-dom";
// import storeConfig from "../../config/storage.config";   

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "Account"
        };
    }

    // componentWillMount() {
    //     if (storeConfig.getUser() !== null) {
    //         this.setState({
    //             email: storeConfig.getUser().email
    //         });
    //     }
    // }

    // componentWillReceiveProps(nextProps) {
    //     if (!nextProps.islogin) {
    //         this.setState({
    //             email: "Account"
    //         });
    //     } else {
    //         this.setState({
    //             email: storeConfig.getUser().email
    //         });
    //     }
    // }

    handlelogin = () => {
        if (this.props.islogin) {
            return (
                <a onClick={() => {
                    window.location.reload();
                    this.props.logout();
                    this.props.history.push("/");
                }} className="login-panel"><i className="fa fa-user"></i>Đăng xuất</a>
            );
        } else {
            return (
                <a href="/login" className="login-panel"><i className="fa fa-user"></i>Đăng nhập</a>
            );
        }
    };

    render() {
        return (
            <header className="header-section">
                <div className="header-top">
                    <div className="container">
                        <div className="ht-left">
                            <div className="mail-service">
                                <i className=" fa fa-envelope"></i>
                                        kovy@gmail.com
                                    </div>
                            <div className="phone-service">
                                <i className=" fa fa-phone"></i>
                                        1900 1080
                                    </div>
                        </div>
                        <div className="ht-right">
                            <a href="/login" className="login-panel"><i className="fa fa-user"></i>Đăng nhập</a>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="inner-header">
                        <div className="row">
                            <div className="col-lg-2 col-md-2">
                                <div className="logo">
                                    <a href="/">
                                        <img style={{ maxWidth: "70%" }} src="img/logo.png" alt=""></img>
                                        {/* <h2>KoVy</h2> */}
                                    </a>
                                </div>
                            </div>
                            <div className="col-lg-7 col-md-7">
                                <div className="advanced-search">
                                    <button type="button" className="category-btn">Tất cả</button>
                                    <div className="input-group">
                                        <input type="text" placeholder="Bạn muốn tìm gì?"></input>
                                        <button type="button"><i className="ti-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-3 text-right col-md-3">
                                <ul className="nav-right">
                                    <li className="heart-icon">
                                        <a href="#">
                                            <i className="icon_heart_alt"></i>
                                            <span>1</span>
                                        </a>
                                    </li>
                                    <li className="cart-icon">
                                        <a href="#">
                                            <i className="icon_bag_alt"></i>
                                            <span>3</span>
                                        </a>
                                        <div className="cart-hover">
                                            <div className="select-items">
                                                <table>
                                                    <tbody>
                                                        <tr>
                                                            <td className="si-pic"><img src="img/select-product-1.jpg" alt=""></img></td>
                                                            <td className="si-text">
                                                                <div className="product-selected">
                                                                    <p>$60.00 x 1</p>
                                                                    <h6>Kabino Bedside Table</h6>
                                                                </div>
                                                            </td>
                                                            <td className="si-close">
                                                                <i className="ti-close"></i>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td className="si-pic"><img src="img/select-product-2.jpg" alt=""></img></td>
                                                            <td className="si-text">
                                                                <div className="product-selected">
                                                                    <p>$60.00 x 1</p>
                                                                    <h6>Kabino Bedside Table</h6>
                                                                </div>
                                                            </td>
                                                            <td className="si-close">
                                                                <i className="ti-close"></i>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="select-total">
                                                <span>total:</span>
                                                <h5>$120.00</h5>
                                            </div>
                                            <div className="select-button">
                                                <a href="#" className="primary-btn view-card">VIEW CARD</a>
                                                <a href="#" className="primary-btn checkout-btn">CHECK OUT</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="cart-price">$150.00</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="nav-item">
                    <div className="container" style={{ maxWidth: "72%" }}>
                        <div className="nav-depart">
                            <div className="depart-btn">
                                <i className="ti-menu"></i>
                                <span>Tất cả</span>
                                <ul className="depart-hover">
                                    <li className="active"><a href="#">Nam</a></li>
                                    <li><a href="#">Nữ</a></li>
                                    <li><a href="#">Trẻ em</a></li>
                                </ul>
                            </div>
                        </div>
                        <nav className="nav-menu mobile-menu">
                            <ul>
                                <li className="active"><a href="/">Trang chủ</a></li>
                                <li><a href="/shop">Cửa hàng</a></li>
                                <li><a href="#">Bộ sưu tập</a>
                                    <ul className="dropdown">
                                        <li><a href="#">Nón</a></li>
                                        <li><a href="#">Áo</a></li>
                                        <li><a href="#">Khăn choàng</a></li>
                                        <li><a href="#">Trang sức</a></li>
                                        <li><a href="#">Túi</a></li>
                                    </ul>
                                </li>
                                <li><a href="./contact">Nam</a></li>
                                <li><a href="./contact">Nữ</a></li>
                                <li><a href="./contact">Trẻ em</a></li>
                                {/* <li><a href="/blog">Blog</a></li> */}
                                <li><a href="./contact">Liên hệ</a></li>
                                <li><a href="#">Trang</a>
                                    <ul className="dropdown">
                                        {/* <li><a href="/blog-details">Blog Details</a></li> */}
                                        <li><a href="/shopping-cart">Giỏ hàng</a></li>
                                        {/* <li><a href="/check-out">Checkout</a></li>
                                                <li><a href="/faq">Faq</a></li> */}
                                        <li><a href="/register">Đăng ký</a></li>
                                        <li><a href="/login">Đăng nhập</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <div id="mobile-menu-wrap"></div>
                    </div>
                </div>
            </header>
        )
    }
}
export default Header