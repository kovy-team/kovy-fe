import React, { Component } from "react";
import Footer from "./footer";
import Header from "./header";

const Register = ({ setEmail, setPassword, setConfirm,
    notificationRegister, registerSubmit, islogin, logout, history }) => (
    <div>
        <Header
            islogin={islogin}
            logout={() => logout}
            history={history}
        />
        {/* Breadcrumb Section Begin */}
        <div class="breacrumb-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-text">
                            <a href="/"><i class="fa fa-home"></i> Trang chủ</a>
                            <span>Đăng ký</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Breadcrumb Form Section Begin */}

        {/* Register Section Begin */}
        <div class="register-login-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="register-form">
                            <h2>Đăng ký</h2>
                            <div className="noti">{notificationRegister}</div>
                            <div class="group-input">
                                <label for="username">Email*</label>
                                <input type="email"
                                    id="username"
                                    placeholder="Email"
                                    onChange={(e) => { setEmail(e.target.value) }} />

                            </div>
                            <div class="group-input">
                                <label for="pass">Mật khẩu*</label>
                                <input type="password"
                                    placeholder="Mật khẩu"
                                    id="pass"
                                    onChange={(e) => setPassword(e.target.value)} />
                            </div>
                            <div class="group-input">
                                <label for="con-pass">Xác nhận mật khẩu*</label>
                                <input type="password"
                                    id="con-pass"
                                    placeholder="Xác nhận mật khẩu"
                                    onChange={(e) => { setConfirm(e.target.value) }} />
                            </div>
                            <button onClick={() => registerSubmit()} class="site-btn register-btn">Đăng ký</button>
                            <div class="switch-login">
                                <a href="./login" class="or-login">Đăng nhập</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Register Form Section End */}

        {/* Partner Logo Section Begin */}
        <div class="partner-logo">
            <div class="container">
                <div class="logo-carousel owl-carousel">
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-1.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-2.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-3.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-4.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-5.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Partner Logo Section End */}
        <Footer />
    </div>
);

export default Register;
