import React, { Component } from "react";
import Footer from "./footer";
import Header from "./header";

const Login = ({ setEmailogin, setPasswordlogin, notificationLogin, loginSubmit }) => (
    <div>
        <Header />
        {/* Breadcrumb Section Begin */}
        <div class="breacrumb-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-text">
                            <a href="#"><i class="fa fa-home"></i> Trang chủ</a>
                            <span>Đăng nhập</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Breadcrumb Form Section Begin */}

        {/* Register Section Begin */}
        <div class="register-login-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="login-form">
                            <h2>Đăng nhập</h2>
                            <div className="noti">{notificationLogin}</div>
                            <div class="group-input">
                                <label for="username">Email address *</label>
                                <input type="email"
                                    id="username"
                                    placeholder="Email address"
                                    onChange={(e) => { setEmailogin(e.target.value) }} />
                            </div>
                            <div class="group-input">
                                <label for="pass">Mật khẩu *</label>
                                <input type="password"
                                    placeholder="Mật khẩu"
                                    onChange={(e) => { setPasswordlogin(e.target.value) }}
                                    id="pass" />
                            </div>
                            <div class="group-input gi-check">
                                <div class="gi-more">
                                    <a href="#" class="forget-pass">Quên mật khẩu</a>
                                </div>
                            </div>
                            <button onClick={() => loginSubmit()} class="site-btn login-btn">Đăng nhập</button>
                            <div class="switch-login">
                                <a href="./register" class="or-login">Tạo tài khoản khác</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Register Form Section End */}

        {/* Partner Logo Section Begin */}
        <div class="partner-logo">
            <div class="container">
                <div class="logo-carousel owl-carousel">
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-1.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-2.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-3.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-4.png" alt="" />
                        </div>
                    </div>
                    <div class="logo-item">
                        <div class="tablecell-inner">
                            <img src="img/logo-carousel/logo-5.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* <Partner Logo Section End */}
        <Footer />
    </div>
);

export default Login;
