import React, { Component } from "react";
import Footer from "./footer";
import Header from "./header";

const Shop = () => (
    <div>
        <Header />
        {/* Breadcrumb Section Begin */}
        <div className="breacrumb-section">
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="breadcrumb-text">
                            <a href="#"><i className="fa fa-home"></i> Home</a>
                            <span>Shop</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Breadcrumb Section Begin */}

        {/* Product Shop Section Begin */}
        <section className="product-shop spad">
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-8 order-2 order-lg-1 produts-sidebar-filter">
                        <div className="filter-widget">
                            <h4 className="fw-title">Loại sản phẩm</h4>
                            <ul className="filter-catagories">
                                <li><a href="#">Nam</a></li>
                                <li><a href="#">Nữ</a></li>
                                <li><a href="#">Trẻ em</a></li>
                            </ul>
                        </div>
                        {/* <div className="filter-widget">
                        <h4 className="fw-title">Price</h4>
                        <div className="filter-range-wrap">
                            <div className="range-slider">
                                <div className="price-input">
                                    <input type="text" id="minamount" />
                                    <input type="text" id="maxamount" />
                                </div>
                            </div>
                            <div className="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                data-min="33" data-max="98">
                                <div className="ui-slider-range ui-corner-all ui-widget-header"></div>
                                <span tabindex="0" className="ui-slider-handle ui-corner-all ui-state-default"></span>
                                <span tabindex="0" className="ui-slider-handle ui-corner-all ui-state-default"></span>
                            </div>
                        </div>
                        <a href="#" className="filter-btn">Filter</a>
                    </div> */}
                        <div className="filter-widget">
                            <h4 className="fw-title">Kích cỡ</h4>
                            <div className="fw-size-choose">
                                <div className="sc-item">
                                    <input type="radio" id="s-size" />
                                    <label for="s-size">s</label>
                                </div>
                                <div className="sc-item">
                                    <input type="radio" id="m-size" />
                                    <label for="m-size">m</label>
                                </div>
                                <div className="sc-item">
                                    <input type="radio" id="l-size" />
                                    <label for="l-size">l</label>
                                </div>
                                <div className="sc-item">
                                    <input type="radio" id="xs-size" />
                                    <label for="xs-size">xs</label>
                                </div>
                            </div>
                        </div>
                        <div className="filter-widget">
                            <h4 className="fw-title">Sản phẩm</h4>
                            <div className="fw-tags">
                                <a href="#">Khăn choàng</a>
                                <a href="#">Áo</a>
                                <a href="#">Túi</a>
                                <a href="#">Trang sức</a>
                                <a href="#">Khác</a>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-9 order-1 order-lg-2">
                        <div className="product-show-option">
                            <div className="row">
                                <div className="col-lg-7 col-md-7">
                                    <div className="select-option">
                                        <select className="nice-select sorting">
                                            <option value="">Default Sorting</option>
                                        </select>
                                        <select className="nice-select p-show open">
                                            <option value="">Show:</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-lg-5 col-md-5 text-right">
                                    <p>Show 01- 09 Of 36 Product</p>
                                </div>
                            </div>
                        </div>
                        <div className="product-list">
                            <div className="row">
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-1.jpg" alt="" />
                                            <div className="sale pp-sale">Sale</div>
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Towel</div>
                                            <a href="#">
                                                <h5>Pure Pineapple</h5>
                                            </a>
                                            <div className="product-price">
                                                $14.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-2.jpg" alt="" />
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Coat</div>
                                            <a href="#">
                                                <h5>Guangzhou sweater</h5>
                                            </a>
                                            <div className="product-price">
                                                $13.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-3.jpg" alt="" />
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Shoes</div>
                                            <a href="#">
                                                <h5>Guangzhou sweater</h5>
                                            </a>
                                            <div className="product-price">
                                                $34.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-4.jpg" alt="" />
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Coat</div>
                                            <a href="#">
                                                <h5>Microfiber Wool Scarf</h5>
                                            </a>
                                            <div className="product-price">
                                                $64.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-5.jpg" alt="" />
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Shoes</div>
                                            <a href="#">
                                                <h5>Men's Painted Hat</h5>
                                            </a>
                                            <div className="product-price">
                                                $44.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-6.jpg" alt="" />
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Shoes</div>
                                            <a href="#">
                                                <h5>Converse Shoes</h5>
                                            </a>
                                            <div className="product-price">
                                                $34.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-7.jpg" alt="" />
                                            <div className="sale pp-sale">Sale</div>
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Towel</div>
                                            <a href="#">
                                                <h5>Pure Pineapple</h5>
                                            </a>
                                            <div className="product-price">
                                                $64.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-8.jpg" alt="" />
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Coat</div>
                                            <a href="#">
                                                <h5>2 Layer Windbreaker</h5>
                                            </a>
                                            <div className="product-price">
                                                $44.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 col-sm-6">
                                    <div className="product-item">
                                        <div className="pi-pic">
                                            <img src="img/products/product-9.jpg" alt="" />
                                            <div className="icon">
                                                <i className="icon_heart_alt"></i>
                                            </div>
                                            <ul>
                                                <li className="w-icon active"><a href="#"><i className="icon_bag_alt"></i></a></li>
                                                <li className="quick-view"><a href="#">+ Quick View</a></li>
                                                <li className="w-icon"><a href="#"><i className="fa fa-random"></i></a></li>
                                            </ul>
                                        </div>
                                        <div className="pi-text">
                                            <div className="catagory-name">Shoes</div>
                                            <a href="#">
                                                <h5>Converse Shoes</h5>
                                            </a>
                                            <div className="product-price">
                                                $34.00
                                            <span>$35.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="loading-more">
                            <i className="icon_loading"></i>
                            <a href="#">
                                Loading More
                        </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        {/* Product Shop Section End */}

        {/* Partner Logo Section Begin  */}
        <div className="partner-logo">
            <div className="container">
                <div className="logo-carousel owl-carousel">
                    <div className="logo-item">
                        <div className="tablecell-inner">
                            <img src="img/logo-carousel/logo-1.png" alt="" />
                        </div>
                    </div>
                    <div className="logo-item">
                        <div className="tablecell-inner">
                            <img src="img/logo-carousel/logo-2.png" alt="" />
                        </div>
                    </div>
                    <div className="logo-item">
                        <div className="tablecell-inner">
                            <img src="img/logo-carousel/logo-3.png" alt="" />
                        </div>
                    </div>
                    <div className="logo-item">
                        <div className="tablecell-inner">
                            <img src="img/logo-carousel/logo-4.png" alt="" />
                        </div>
                    </div>
                    <div className="logo-item">
                        <div className="tablecell-inner">
                            <img src="img/logo-carousel/logo-5.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {/* Partner Logo Section End */}
        <Footer />
    </div>
);

export default Shop;
