import React, { Component } from "react";
import Footer from "../footer";
import Header from "../header";
import "./home.css";

const Home = () => (
  <div>
    <Header />
    {/* Hero Section Begin */}
    <section className="hero-section">
      <div
        id="carouselExampleIndicators"
        class="carousel slide"
        data-ride="carousel"
      >
        <ol class="carousel-indicators">
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="0"
            class="active"
            style={{ backgroundColor: "rgb(124, 124, 120)" }}
          ></li>
          <li
            data-target="#carouselExampleIndicators"
            data-slide-to="1"
            style={{ backgroundColor: "rgb(124, 124, 120)" }}
          ></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="img/hero-1.jpg" alt="First slide" />
          </div>
          <div class="carousel-item">
            <img
              class="d-block w-100"
              src="img/hero-2.jpg"
              alt="Second slide"
            />
          </div>
        </div>
        <a
          class="carousel-control-prev"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="prev"
        >
          <img src="img/left-arrow.png" alt="Second slide" />
          <span class="sr-only">Previous</span>
        </a>
        <a
          class="carousel-control-next"
          href="#carouselExampleIndicators"
          role="button"
          data-slide="next"
        >
          <img src="img/arrow-point-to-right.png" alt="Second slide" />
          <span class="sr-only">Next</span>
        </a>
      </div>
    </section>

    {/* Hero Section End */}

    {/* Banner Section Begin */}
    <div className="banner-section spad">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-4">
            <div className="single-banner">
              <img src="img/banner-1.jpg" alt=""></img>
              <div className="inner-text">
                <h4>Men’s</h4>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="single-banner">
              <img src="img/banner-2.jpg" alt=""></img>
              <div className="inner-text">
                <h4>Women’s</h4>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="single-banner">
              <img src="img/banner-3.jpg" alt=""></img>
              <div className="inner-text">
                <h4>Kid’s</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {/* Banner Section End */}

    {/* Women Banner Section Begin */}
    <section className="women-banner spad">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-3">
            <div
              className="product-large set-bg"
              data-setbg="img/products/women-large.jpg"
            >
              <h2>Women’s</h2>
              <a href="#">Discover More</a>
            </div>
          </div>
          <div className="col-lg-8 offset-lg-1">
            <div className="filter-control">
              <ul>
                <li className="active">Áo</li>
                <li>Khăn choàng</li>
                <li>Túi</li>
                <li>Trang sức</li>
              </ul>
            </div>

            <div class="container">
              <div class="row">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li
                      data-target="#carousel"
                      data-slide-to="0"
                      class="active"
                    ></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <div class="d-none d-lg-block">
                        <div class="slide-box">
                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-1.jpg"
                                  alt=""
                                ></img>
                                <div className="sale">Sale</div>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Shoes</div>
                                <a href="#">
                                  <h5>Guangzhou sweater</h5>
                                </a>
                                <div className="product-price">$13.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-2.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Towel</div>
                                <a href="#">
                                  <h5>Converse Shoes</h5>
                                </a>
                                <div className="product-price">$34.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-3.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Coat</div>
                                <a href="#">
                                  <h5>Pure Pineapple</h5>
                                </a>
                                <div className="product-price">
                                  $14.00
                                  <span>$35.00</span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-4.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="pi-text">
                              <div className="catagory-name">Towel</div>
                              <a href="#">
                                <h5>Pure Pineapple</h5>
                              </a>
                              <div className="product-price">$34.00</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="d-none d-lg-block">
                        <div class="slide-box">
                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-1.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Shoes</div>
                                <a href="#">
                                  <h5>Guangzhou sweater</h5>
                                </a>
                                <div className="product-price">$13.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-2.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Towel</div>
                                <a href="#">
                                  <h5>Converse Shoes</h5>
                                </a>
                                <div className="product-price">$34.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-3.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Coat</div>
                                <a href="#">
                                  <h5>Pure Pineapple</h5>
                                </a>
                                <div className="product-price">
                                  $14.00
                                  <span>$35.00</span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-4.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="pi-text">
                              <div className="catagory-name">Towel</div>
                              <a href="#">
                                <h5>Pure Pineapple</h5>
                              </a>
                              <div className="product-price">$34.00</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a
                    class="carousel-control-prev"
                    href="#carousel"
                    role="button"
                    data-slide="prev"
                    style={{ marginLeft: "-11%", marginBottom: "13%" }}
                  >
                    <img src="img/left-arrow.png" alt="Second slide" />
                    <span class="sr-only">Previous</span>
                  </a>
                  <a
                    class="carousel-control-next"
                    href="#carousel"
                    role="button"
                    data-slide="next"
                    style={{ marginRight: "-10%", marginBottom: "13%" }}
                  >
                    <img
                      src="img/arrow-point-to-right.png"
                      alt="Second slide"
                    />
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* Women Banner Section End */}
    {/* Deal Of The Week Section Begin */}
    {/* <section className="deal-of-week set-bg spad" data-setbg="img/time-bg.jpg">
      <div className="container">
        <div className="col-lg-6 text-center">
          <div className="section-title">
            <h2>Deal Of The Week</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
              <br /> do ipsum dolor sit amet, consectetur adipisicing elit{" "}
            </p>
            <div className="product-price">
              $35.00
              <span>/ HanBag</span>
            </div>
          </div>
          <div className="countdown-timer" id="countdown">
            <div className="cd-item">
              <span>56</span>
              <p>Days</p>
            </div>
            <div className="cd-item">
              <span>12</span>
              <p>Hrs</p>
            </div>
            <div className="cd-item">
              <span>40</span>
              <p>Mins</p>
            </div>
            <div className="cd-item">
              <span>52</span>
              <p>Secs</p>
            </div>
          </div>
          <a href="#" className="primary-btn">
            Shop Now
          </a>
        </div>
      </div>
    </section> */}
    {/* Deal Of The Week Section End */}

    {/* Man Banner Section Begin */}
    <section className="man-banner spad">
      <div className="container-fluid">
        <div className="row">
          <div className="col-lg-8">
            <div className="filter-control">
              <ul>
                <li className="active">Áo</li>
                <li>Khăn choàng</li>
                <li>Túi</li>
                <li>Trang sức</li>
              </ul>
            </div>
            <div class="container">
              <div class="row">
                <div id="carousel1" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li
                      data-target="#carousel"
                      data-slide-to="0"
                      class="active"
                    ></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <div class="d-none d-lg-block">
                        <div class="slide-box">
                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-1.jpg"
                                  alt=""
                                ></img>
                                <div className="sale">Sale</div>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Shoes</div>
                                <a href="#">
                                  <h5>Guangzhou sweater</h5>
                                </a>
                                <div className="product-price">$13.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-2.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Towel</div>
                                <a href="#">
                                  <h5>Converse Shoes</h5>
                                </a>
                                <div className="product-price">$34.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-3.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Coat</div>
                                <a href="#">
                                  <h5>Pure Pineapple</h5>
                                </a>
                                <div className="product-price">
                                  $14.00
                                  <span>$35.00</span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-4.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="pi-text">
                              <div className="catagory-name">Towel</div>
                              <a href="#">
                                <h5>Pure Pineapple</h5>
                              </a>
                              <div className="product-price">$34.00</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="carousel-item">
                      <div class="d-none d-lg-block">
                        <div class="slide-box">
                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-1.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Shoes</div>
                                <a href="#">
                                  <h5>Guangzhou sweater</h5>
                                </a>
                                <div className="product-price">$13.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-2.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Towel</div>
                                <a href="#">
                                  <h5>Converse Shoes</h5>
                                </a>
                                <div className="product-price">$34.00</div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-3.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div className="pi-text">
                                <div className="catagory-name">Coat</div>
                                <a href="#">
                                  <h5>Pure Pineapple</h5>
                                </a>
                                <div className="product-price">
                                  $14.00
                                  <span>$35.00</span>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="owl-item" style={{ width: "60%" }}>
                            <div className="product-item">
                              <div className="pi-pic">
                                <img
                                  src="img/products/women-4.jpg"
                                  alt=""
                                ></img>
                                <div className="icon">
                                  <i className="icon_heart_alt"></i>
                                </div>
                                <ul>
                                  <li className="w-icon active">
                                    <a href="#">
                                      <i className="icon_bag_alt"></i>
                                    </a>
                                  </li>
                                  <li className="quick-view">
                                    <a href="#">+ Quick View</a>
                                  </li>
                                  <li className="w-icon">
                                    <a href="#">
                                      <i className="fa fa-random"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div className="pi-text">
                              <div className="catagory-name">Towel</div>
                              <a href="#">
                                <h5>Pure Pineapple</h5>
                              </a>
                              <div className="product-price">$34.00</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a
                    class="carousel-control-prev"
                    href="#carousel1"
                    role="button"
                    data-slide="prev"
                    style={{ marginLeft: "-11%", marginBottom: "13%" }}
                  >
                    <img src="img/left-arrow.png" alt="Second slide" />
                    <span class="sr-only">Previous</span>
                  </a>
                  <a
                    class="carousel-control-next"
                    href="#carousel1"
                    role="button"
                    data-slide="next"
                    style={{ marginRight: "-10%", marginBottom: "13%" }}
                  >
                    <img
                      src="img/arrow-point-to-right.png"
                      alt="Second slide"
                    />
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 offset-lg-1">
            <div
              className="product-large set-bg m-large"
              data-setbg="img/products/man-large.jpg"
            >
              <h2>Men’s</h2>
              <a href="#">Discover More</a>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* Man Banner Section End */}

    {/* Instagram Section Begin */}
    <div className="instagram-photo">
      <div className="insta-item set-bg" data-setbg="img/insta-1.jpg">
        <div className="inside-text">
          <i className="ti-instagram"></i>
          <h5>
            <a href="#">colorlib_Collection</a>
          </h5>
        </div>
      </div>
      <div className="insta-item set-bg" data-setbg="img/insta-2.jpg">
        <div className="inside-text">
          <i className="ti-instagram"></i>
          <h5>
            <a href="#">colorlib_Collection</a>
          </h5>
        </div>
      </div>
      <div className="insta-item set-bg" data-setbg="img/insta-3.jpg">
        <div className="inside-text">
          <i className="ti-instagram"></i>
          <h5>
            <a href="#">colorlib_Collection</a>
          </h5>
        </div>
      </div>
      <div className="insta-item set-bg" data-setbg="img/insta-4.jpg">
        <div className="inside-text">
          <i className="ti-instagram"></i>
          <h5>
            <a href="#">colorlib_Collection</a>
          </h5>
        </div>
      </div>
      <div className="insta-item set-bg" data-setbg="img/insta-5.jpg">
        <div className="inside-text">
          <i className="ti-instagram"></i>
          <h5>
            <a href="#">colorlib_Collection</a>
          </h5>
        </div>
      </div>
      <div className="insta-item set-bg" data-setbg="img/insta-6.jpg">
        <div className="inside-text">
          <i className="ti-instagram"></i>
          <h5>
            <a href="#">colorlib_Collection</a>
          </h5>
        </div>
      </div>
    </div>
    {/* Instagram Section End */}

    {/* Latest Blog Section Begin */}
    <section className="latest-blog spad">
      <div className="container">
        {/* <div className="row">
          <div className="col-lg-12">
            <div className="section-title">
              <h2>From The Blog</h2>
            </div>
          </div>
        </div> */}
        {/* <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="single-latest-blog">
              <img src="img/latest-1.jpg" alt=""></img>
              <div className="latest-text">
                <div className="tag-list">
                  <div className="tag-item">
                    <i className="fa fa-calendar-o"></i>
                      May 4,2019
                    </div>
                  <div className="tag-item">
                    <i className="fa fa-comment-o"></i>5
                    </div>
                </div>
                <a href="#">
                  <h4>The Best Street Style From London Fashion Week</h4>
                </a>
                <p>
                  Sed quia non numquam modi tempora indunt ut labore et dolore
                    magnam aliquam quaerat{" "}
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="single-latest-blog">
              <img src="img/latest-2.jpg" alt=""></img>
              <div className="latest-text">
                <div className="tag-list">
                  <div className="tag-item">
                    <i className="fa fa-calendar-o"></i>
                      May 4,2019
                    </div>
                  <div className="tag-item">
                    <i className="fa fa-comment-o"></i>5
                    </div>
                </div>
                <a href="#">
                  <h4>Vogue's Ultimate Guide To Autumn/Winter 2019 Shoes</h4>
                </a>
                <p>
                  Sed quia non numquam modi tempora indunt ut labore et dolore
                    magnam aliquam quaerat{" "}
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="single-latest-blog">
              <img src="img/latest-3.jpg" alt=""></img>
              <div className="latest-text">
                <div className="tag-list">
                  <div className="tag-item">
                    <i className="fa fa-calendar-o"></i>
                      May 4,2019
                    </div>
                  <div className="tag-item">
                    <i className="fa fa-comment-o"></i>5
                    </div>
                </div>
                <a href="#">
                  <h4>How To Brighten Your Wardrobe With A Dash Of Lime</h4>
                </a>
                <p>
                  Sed quia non numquam modi tempora indunt ut labore et dolore
                    magnam aliquam quaerat{" "}
                </p>
              </div>
            </div>
          </div>
        </div> */}
        <div className="benefit-items">
          <div className="row">
            <div className="col-lg-4">
              <div className="single-benefit">
                <div className="sb-icon">
                  <img src="img/icon-1.png" alt=""></img>
                </div>
                <div className="sb-text">
                  <h6>Free Shipping</h6>
                  <p>For all order over 99$</p>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="single-benefit">
                <div className="sb-icon">
                  <img src="img/icon-2.png" alt=""></img>
                </div>
                <div className="sb-text">
                  <h6>Delivery On Time</h6>
                  <p>If good have prolems</p>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="single-benefit">
                <div className="sb-icon">
                  <img src="img/icon-1.png" alt=""></img>
                </div>
                <div className="sb-text">
                  <h6>Secure Payment</h6>
                  <p>100% secure payment</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {/* Latest Blog Section End */}
    {/* Partner Logo Section Begin */}
    <div className="partner-logo">
      <div className="container">
        <div className="logo-carousel owl-carousel">
          <div className="logo-item">
            <div className="tablecell-inner">
              <img src="img/logo-carousel/logo-1.png" alt=""></img>
            </div>
          </div>
          <div className="logo-item">
            <div className="tablecell-inner">
              <img src="img/logo-carousel/logo-2.png" alt=""></img>
            </div>
          </div>
          <div className="logo-item">
            <div className="tablecell-inner">
              <img src="img/logo-carousel/logo-3.png" alt=""></img>
            </div>
          </div>
          <div className="logo-item">
            <div className="tablecell-inner">
              <img src="img/logo-carousel/logo-4.png" alt=""></img>
            </div>
          </div>
          <div className="logo-item">
            <div className="tablecell-inner">
              <img src="img/logo-carousel/logo-5.png" alt=""></img>
            </div>
          </div>
        </div>
      </div>
    </div>
    {/* Partner Logo Section End */}
    <Footer />
  </div>
);

export default Home;
